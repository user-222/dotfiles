set nocompatible              " be iMproved, required
filetype off                  " required
source $VIMRUNTIME/vimrc_example.vim

set noerrorbells
set belloff=all
set number
set tabstop=4
set shiftwidth=4
set expandtab
set hlsearch
syntax on
syntax enable
colorscheme torte

vnoremap <leader>p "_dP

if has('win32')
    set termguicolors
    set shellslash
    set t_ut=""
    set rtp+=~/vimfiles/bundle/Vundle.vim
else
    set rtp+=~/.vim/bundle/Vundle.vim
endif

call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'vim-airline/vim-airline'
Plugin 'dense-analysis/ale'
Plugin 'preservim/nerdtree'
Plugin 'Vimjas/vim-python-pep8-indent'
Plugin 'sheerun/vim-polyglot'

if !has('win32')
    Plugin 'mileszs/ack.vim'
endif

call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" Airline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'default'
let g:airline#extensions#ale#enabled = 1

set noshowmode
set noshowcmd
set shortmess+=F

" Ackvim
if executable('ag')
    let g:ackprg = 'ag --vimgrep'
endif

" Tmux Fixes
if &term =~ '^screen'
    " tmux will send xterm-style keys when its xterm-keys option is on
    execute "set <xUp>=\e[1;*A"
    execute "set <xDown>=\e[1;*B"
    execute "set <xRight>=\e[1;*C"
    execute "set <xLeft>=\e[1;*D"
endif

" NERDTreee
let NERDTreeShowHidden=1
map <TAB> :NERDTreeToggle<CR>
set timeoutlen=1000 ttimeoutlen=100

" Mouse
set ttymouse=xterm2
set mouse=a

" Undo
set noundofile
set nobackup

" Ale
let g:ale_python_pylint_change_directory=0
let g:ale_python_flake8_change_directory=0
