function Get-GitStatusPriv { & git status $args }
New-Alias -Name gs -Value Get-GitStatusPriv -Force -Option AllScope
function Get-GitCommit { & git commit $args }
New-Alias -Name gc -Value Get-GitCommit -Force -Option AllScope
function Get-GitAdd { & git add $args }
New-Alias -Name ga -Value Get-GitAdd -Force -Option AllScope
function Get-GitTree { & git log --graph --oneline --decorate $args }
New-Alias -Name gt -Value Get-GitTree -Force -Option AllScope
function Get-GitPush { & git push $args }
New-Alias -Name gp -Value Get-GitPush -Force -Option AllScope
function Get-GitPull { & git pull $args }
New-Alias -Name gpu -Value Get-GitPull -Force -Option AllScope
function Get-GitFetch { & git fetch $args }
New-Alias -Name gf -Value Get-GitFetch -Force -Option AllScope
function Get-GitFetchAll { & git fetch --all $args }
New-Alias -Name gfa -Value Get-GitFetchAll -Force -Option AllScope
function Get-GitCheckout { & git checkout $args }
New-Alias -Name gco -Value Get-GitCheckout -Force -Option AllScope
function Get-GitCheckoutB { & git checkout -b $args }
function Get-GitBranch { & git branch $args }
New-Alias -Name gb -Value Get-GitBranch -Force -Option AllScope
function Get-GitRemote { & git remote -v $args }
New-Alias -Name gr -Value Get-GitRemote -Force -Option AllScope

Set-PoshPrompt -Theme ~/Documents/WindowsPowerShell/.oh-my-posh.omp.json
Set-PSReadlineKeyHandler -Key Tab -Function Complete
Import-Module Get-ChildItemColor
New-Alias -Name ls -Value Get-ChildItemColorFormatWide -Force -Option AllScope
New-Alias -Name gcb -Value Get-GitCheckoutB -Force
New-Alias -Name which -Value Get-Command
$env:VIRTUAL_ENV_DISABLE_PROMPT=1
