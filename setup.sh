#!/bin/sh

# Packages
sudo apt update && sudo apt upgrade -y
sudo apt install -y tmux git xclip tig zsh build-essential default-jdk default-jre unzip

# Switch to zsh
chsh -s $(which zsh)
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# Install and configure neovim
~/.dotfiles/nvim.sh

# Install fzf
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install

# Symlinks
ln -sf ~/.dotfiles/.tmux.conf ~/.tmux.conf
ln -sf ~/.dotfiles/.xinitrc ~/.xinitrc
ln -sf ~/.dotfiles/.Xresources ~/.Xresources
ln -sf ~/.dotfiles/.aliases ~/.aliases
ln -sf ~/.dotfiles/.zshrc ~/.zshrc
mkdir -p ~/.config
if [ -e ~/.config/i3 ]; then
    rm -rf ~/.config/i3
fi
ln -s ~/.dotfiles/i3 ~/.config/i3
mkdir -p ~/.local/bin
echo "source ~/.aliases" >> ~/.bashrc
ln -sf ~/.dotfiles/custom.zsh-theme ~/.oh-my-zsh/themes/custom.zsh-theme

# git config
git config --global core.editor vim
