#!/bin/bash
set -e

# First, remove neovim if it exists
if apt list --installed 2>&1 | grep "neovim"; then
	echo "[*] Neovim apt installed, removing"
	sudo apt remove -y neovim neovim-runtime
	rm -rf ~/.local/share/nvim/lazy
elif [ -d /opt/nvim-linux-x86_64 ]; then
	echo "[*] Neovim installed in opt"
	sudo rm -rf /opt/nvim-linux-x86_64
	rm -rf ~/.local/share/nvim/lazy
else
	echo "[*] Neovim not installed"
fi

# Download and install "latest"
pushd /tmp
curl -s https://api.github.com/repos/neovim/neovim/releases/latest \
	| grep "nvim-linux-x86_64.tar.gz" \
	| cut -d : -f 2,3 \
	| grep -v sha256 \
	| grep github \
	| tr -d \" \
	| tr -s " " \
	| wget -qi -
pushd /opt
sudo tar xzvf /tmp/nvim-linux-x86_64.tar.gz
sudo ln -sf /opt/nvim-linux-x86_64/bin/nvim /usr/local/bin/nvim
popd

# Download lazy.nvim
mkdir -p ~/.local/share/nvim/lazy
pushd ~/.local/share/nvim/lazy
git clone https://github.com/folke/lazy.nvim.git
popd

# Symlinks
ln -sf ~/.dotfiles/.vimrc ~/.vimrc
if [ -e ~/.config/nvim ] || [ -d ~/.config/nvim ]; then
    echo "[*] Neovim config already present, removing"
    rm -rf ~/.config/nvim
fi
ln -sf ~/.dotfiles/nvim ~/.config/nvim

# Sync Plugins
nvim --headless -E "+Lazy! sync" +"TSUpdateSync" +qa
